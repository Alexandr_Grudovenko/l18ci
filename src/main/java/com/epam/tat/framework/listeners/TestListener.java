package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        super.onTestFailure(iTestResult);
        Log.info(String.format("[TEST FAILURE] : %s", iTestResult.getName()));
        Log.debug("Closing WebDriver and all associated windows");
        Browser.screenshot();
        Browser.getInstance().getWrappedDriver().quit();
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        super.onStart(iTestContext);
        Log.info(String.format("[TEST STARTED] : %s", iTestContext.getName()));
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        super.onFinish(iTestContext);
        Log.info(String.format("[TEST FINISHED] : %s", iTestContext.getName()));
    }
}
