package com.epam.tat.framework.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RandomUtil {
    public static final String DYNAMIC_FILE_NAME = "./src/generatedFiles/uploadThisFile %s.txt";
    public static final String ERROR_MESSAGE = "File %s not found!!!";
    public static final int ALPHANUMERIC_COUNT = 8;

    public static String getRandomFolder() {
        return "My Folder " + new Random().nextInt();
    }

    public static File getRandomFile() {
        File file = new File(String.format(DYNAMIC_FILE_NAME, String.valueOf(new Random().nextInt())));
        try {
            FileUtils.writeStringToFile(file, "Hello world!", "UTF-8");
        } catch (IOException e) {
            System.out.println(String.format(ERROR_MESSAGE, file.getAbsolutePath()));
        }
        return file;
    }

    public static String getRandomLogin() {
        return RandomStringUtils.randomAlphanumeric(ALPHANUMERIC_COUNT);
    }

    public static String getRandomPassword() {
        return RandomStringUtils.randomAlphanumeric(ALPHANUMERIC_COUNT);
    }

    public static String getRandomSubject() {
        return "theme" + Math.random();
    }

    public static String getRandomMailText() {
        return RandomStringUtils.randomAlphanumeric(ALPHANUMERIC_COUNT);
    }
}
