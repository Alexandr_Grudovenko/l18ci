package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class Browser implements WrapsDriver {
    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private WebDriver wrappedWebDriver;
    private final int waitingTimeout = 20;

    private Browser() {
        this.wrappedWebDriver = WebDriverFactory.getWebDriver();
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
            Log.debug("[Browser opened]");
        }
        return instance.get();
    }

    public void stopBrowser() {
        try {
            if (wrappedWebDriver != null) {
                wrappedWebDriver.quit();
            }
        } finally {
            instance.set(null);
        }
        Log.debug("[Browser stopped]");
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void navigate(String url) {
        wrappedWebDriver.get(url);
        Log.debug(String.format("Open URL: %s", url));
    }

    public void click(By by) {
        waitForAppear(by);
        Log.debug(String.format("Click on element %s", by));
        wrappedWebDriver
                .findElement(by)
                .click();
    }

    public void type(By by, String text) {
        waitForAppear(by);
        Log.debug(String.format("Type %s in web element: %s ", text, by));
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
    }

    public void clear(By by) {
        waitForAppear(by);
        Log.debug(String.format("Clear element: %s ", by));
        wrappedWebDriver
                .findElement(by)
                .clear();
    }

    public void switchTo(By by) {
        waitForAppear(by);
        Log.debug(String.format("Switch to element: %s ", by));
        wrappedWebDriver
                .switchTo().frame(wrappedWebDriver.findElement(by));
    }

    public String getText(By by) {
        waitForAppear(by);
        Log.debug(String.format("Get text from element: %s ", by));
        return wrappedWebDriver
                .findElement(by)
                .getText();
    }

    public void implicitlyWait() {
        wrappedWebDriver.manage().timeouts().implicitlyWait(waitingTimeout, TimeUnit.SECONDS);
    }

    public void waitForAppear(By by) {
        WebDriverWait wait = new WebDriverWait(wrappedWebDriver, waitingTimeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void dragAndDrop(WebElement element, WebElement destinationElement) {
        Log.debug(String.format("Drag and drop element: %s ", element.getText()));
        new Actions(wrappedWebDriver)
                .dragAndDrop(element, destinationElement)
                .build()
                .perform();
    }

    public void rightClick(By by) {
        Log.debug(String.format("Right click on element: %s ", by));
        new Actions(wrappedWebDriver)
                .contextClick(Browser.getInstance().getWrappedDriver().findElement(by))
                .perform();
    }

    public boolean isVisible(By by) {
        return wrappedWebDriver.findElement(by).isDisplayed();
    }

    public static void screenshot() {
        File screenshotFile = new File(String.format("screenshots/screenshot%s.png", System.nanoTime()));
        try {
            byte[] screenshotBytes = ((TakesScreenshot) Browser.getInstance()
                    .getWrappedDriver()).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("Screenshot taken. File: " + screenshotFile.getCanonicalPath());
            String screenshotImgTag = String.format("<img src=%s>", screenshotFile.getCanonicalPath());
            Reporter.log("<a href=" + screenshotFile.getCanonicalPath() + " target='blank' >"
                    + screenshotImgTag + "</a>");
        } catch (IOException e) {
            System.out.println("Failed to write screenshot! " + e);
        }
    }
}
