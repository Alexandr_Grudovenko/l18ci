package com.epam.tat.framework.ui;

import org.openqa.selenium.By;

public class Element {
    private By locator;
    private final int sleepTimeout = 2000;

    public Element(By locator) {
        this.locator = locator;
    }

    public void click() {
        try {
            Thread.sleep(sleepTimeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Browser.getInstance().click(locator);
    }

    public void type(String text) {
        Browser.getInstance().type(locator, text);
    }

    public String getText() {
        return Browser.getInstance().getText(locator);
    }

    public void waitForAppear() {
        Browser.getInstance().implicitlyWait();
    }

    public boolean isDisplayed() {
        try {
            Thread.sleep(sleepTimeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Browser.getInstance().isVisible(locator);
    }

    public void switchToDefaultContent() {
        Browser.getInstance().getWrappedDriver().switchTo().defaultContent();
    }

    public void switchToFrame() {
        Browser.getInstance().switchTo(locator);
    }

    public void dragAndDropTo(By destination) {
        Browser.getInstance().dragAndDrop(Browser.getInstance().getWrappedDriver().findElement(locator),
                Browser.getInstance().getWrappedDriver().findElement(destination));
    }

    public void rightClick() {
        Browser.getInstance().rightClick(locator);
    }

    public String getCssValue(String cssValue) {
        return Browser.getInstance().getWrappedDriver().findElement(locator).getCssValue(cssValue);
    }
}
