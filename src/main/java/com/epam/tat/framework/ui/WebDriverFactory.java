package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverFactory {

    public static WebDriver getWebDriver() {
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
                webDriver = new ChromeDriver();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());
        }
        return webDriver;
    }
}
