package com.epam.tat.cloud.page;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class LoginPage {
    private static String url = "https://cloud.mail.ru/";
    private static Element mainSignInButton =
            new Element(xpath("//input[@type='button' and @class='nav-inner-col-try__bt']"));
    private static Element loginField = new Element(xpath("//input[@id='ph_login']"));
    private static Element passwordField = new Element(xpath("//input[@id='ph_password']"));
    private static Element signInButton = new Element(xpath("//span[@data-action='login']"));
    private static Element passwordFieldBorder = new Element(xpath("//label[@for='ph_password']"));
    private static Element loginFieldBorder = new Element(xpath("//label[@for='ph_login']"));

    public LoginPage clickMainSignIn() {
        mainSignInButton.click();
        return this;
    }

    public LoginPage open() {
        Browser.getInstance().navigate(url);
        return this;
    }

    public LoginPage setLogin(String login) {
        loginField.type(login);
        return this;
    }

    public LoginPage setPassword(String password) {
        passwordField.type(password);
        return this;
    }

    public CloudMainPage clickSignIn() {
        signInButton.click();
        return new CloudMainPage();
    }

    public String getPasswordBorderColor() {
        return passwordFieldBorder.getCssValue("border-color");
    }

    public String getLoginBorderColor() {
        return loginFieldBorder.getCssValue("border-color");
    }
}
