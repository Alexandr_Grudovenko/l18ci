package com.epam.tat.cloud.page;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class SharedFilesPage {
    private static Element sharedFile =
            new Element(xpath("//div[@class='viewer__content viewer__content_icon']//a[@class='viewer__information__link']"));

    public String getSharedFile() {
        return sharedFile.getText();
    }
}
