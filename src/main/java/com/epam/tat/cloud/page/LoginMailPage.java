package com.epam.tat.cloud.page;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class LoginMailPage {

    private static Element loginError = new Element(xpath("//div[@class='b-login__errors']"));
    private static Element frame = new Element(xpath("//iframe[@class='ag-popup__frame__layout__iframe']"));

    public String getErrorMessage() {
        frame.switchToFrame();
        return loginError.getText();
    }
}

