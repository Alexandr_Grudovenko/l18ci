package com.epam.tat.cloud.page;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;
import java.io.File;

import static org.openqa.selenium.By.xpath;

public class CloudMainPage {
    private static String dynamicFolderLocator = "//div[@data-id='/%s' and @data-name='link']";
    private static String dynamicFileLocator
            = "//div[@data-id='/%s' and @data-name='link']//div[@class='b-thumb__content']";
    private static String dynamicCheckboxLocator = "//div[@data-id='/%s']//div[@class='b-checkbox__box']";
    private static String currentElementCheckbox = "//div[@data-id='/%s']//input[@class='b-checkbox__input']";
    private Element userName = new Element(xpath("//i[@id='PH_user-email']"));
    private Element createButton = new Element(xpath("//div[@id='toolbar-left']//div[@data-group='create']"));
    private Element createFolderButton =
            new Element(xpath("//a[@class='b-dropdown__list__item' and @data-name='folder']"));
    private Element folderNameField = new Element(xpath("//div[@id='app']//input[@class='layer__input']"));
    private Element addFolderButton = new Element(xpath("//div[@id='app']//button[@data-name='add']"));
    private Element deleteButton = new Element(xpath("//*[@id='toolbar']//div[@data-name='remove']"));
    private Element uploadButton = new Element(xpath("//div[@id='toolbar-left']//div[@data-name='upload']"));
    private Element selectFile = new Element(xpath("//div[@id='app']//input[@class='layer_upload__controls__input']"));
    private Element acceptButton = new Element(xpath("//button[@data-name='move']"));
    private Element shareButton = new Element(xpath("//a[@data-name='publish']"));
    private Element viewButton = new Element(xpath("//div[@id='app']//button[@data-name='popup']"));
    private Element dialogDeleteButton = new Element(xpath("//div[@id='app']//button[@data-name='remove']"));

    public CloudMainPage createButtonClick() {
        createButton.click();
        return this;
    }

    public CloudMainPage createFolderButtonClick() {
        createFolderButton.click();
        return this;
    }

    public CloudMainPage setFolderName(String folderName) {
        folderNameField.type(folderName);
        return this;
    }

    public CloudMainPage addFolderButtonClick() {
        addFolderButton.click();
        return this;
    }

    public CloudMainPage acceptMoveButtonClick() {
        acceptButton.click();
        return this;
    }

    public CloudMainPage dialogDeleteButtonClick() {
        dialogDeleteButton.click();
        return this;
    }

    public CloudMainPage destinationFolderClick(String folderName) {
        Element destinationFolder = new Element(xpath(String.format(CloudMainPage.dynamicFolderLocator, folderName)));
        destinationFolder.click();
        return this;
    }

    public CloudMainPage folderCheckBoxClick(String folderName) {
        Element checkBox = new Element(xpath(String.format(dynamicCheckboxLocator, folderName)));
        checkBox.click();
        return this;
    }

    public CloudMainPage deleteButtonClick() {
        deleteButton.click();
        return this;
    }

    public CloudMainPage uploadButtonClick() {
        uploadButton.click();
        return this;
    }

    public CloudMainPage sendFile(File file) {
        selectFile.type(file.getAbsolutePath());
        return this;
    }

    public CloudMainPage shareFile() {
        shareButton.waitForAppear();
        shareButton.click();
        return this;
    }

    public void viewFileLinkClick() {
        viewButton.waitForAppear();
        viewButton.click();
    }

    public String findUploadedFileName(File file) {
        Element dynamicFile = new Element(xpath(String.format(dynamicFileLocator, file.getName())));
        if (dynamicFile.isDisplayed()) {
            return file.getName();
        } else {
            return null;
        }
    }

    public Element getElementFile(File file) {
        Element dynamicFile = new Element(xpath(String.format(dynamicFileLocator, file.getName())));
        return dynamicFile;
    }

    public By getFolder(String folder) {
        By dynamicFolder = By.xpath(String.format(dynamicFolderLocator, folder));
        return dynamicFolder;
    }

    public boolean isFolderCreated(String folderName) {
        Element dynamicFolder = new Element(xpath(String.format(dynamicFolderLocator, folderName)));
        return dynamicFolder.isDisplayed();
    }

    public boolean isFolderExist(String folderName) {
        Element checkBox = new Element(xpath(String.format(currentElementCheckbox, folderName)));
        return checkBox.isDisplayed();
    }

    public boolean getUserName() {
        return userName.isDisplayed();
    }
}
