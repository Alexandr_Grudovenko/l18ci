package com.epam.tat.cloud.exception;

public class WorkWithFolderAndFileException extends RuntimeException {
    public WorkWithFolderAndFileException(String message) {
        super(message);
    }
}
