package com.epam.tat.cloud.service;

import com.epam.tat.cloud.exception.WorkWithFolderAndFileException;
import com.epam.tat.cloud.page.CloudMainPage;
import com.epam.tat.cloud.page.SharedFilesPage;
import com.epam.tat.framework.ui.Browser;
import java.io.File;

public class FileService {

    public void createFolder(String folder) {
        CloudMainPage cloudMainPage = new CloudMainPage();
        cloudMainPage
                .createButtonClick()
                .createFolderButtonClick()
                .setFolderName(folder)
                .addFolderButtonClick();
        if (isFolderCreated(folder)) {
            throw new WorkWithFolderAndFileException("Folder not created!");
        }
    }

    public void deleteFolder(String folder) {
        CloudMainPage cloudMainPage = new CloudMainPage();
        cloudMainPage
                .folderCheckBoxClick(folder)
                .deleteButtonClick()
                .dialogDeleteButtonClick();
        if (!isFolderExist(folder)) {
            throw new WorkWithFolderAndFileException("Folder doesn't exist!");
        }
    }

    public void uploadFileToTheRootDirectory(File file) {
        CloudMainPage cloudMainPage = new CloudMainPage();
        cloudMainPage
                .uploadButtonClick()
                .sendFile(file);
        if (!file.getName().equals(cloudMainPage.findUploadedFileName(file))) {
            throw new WorkWithFolderAndFileException("File uploading error!");
        }
    }

    public void dragAndDropFileToFolder(File file, String folder) {
        CloudMainPage cloudMainPage = new CloudMainPage();
        cloudMainPage.getElementFile(file).dragAndDropTo(cloudMainPage.getFolder(folder));
        cloudMainPage
                .acceptMoveButtonClick();
    }

    public void shareFile(File file) {
        CloudMainPage cloudMainPage = new CloudMainPage();
        cloudMainPage.getElementFile(file).rightClick();
        cloudMainPage
                .shareFile()
                .viewFileLinkClick();
        for (String winHandle : Browser.getInstance().getWrappedDriver().getWindowHandles()) {
            Browser.getInstance().getWrappedDriver().switchTo().window(winHandle);
        }
        if (!file.getName().equals(new SharedFilesPage().getSharedFile())) {
            throw new WorkWithFolderAndFileException("Can't share file!");
        }
    }

    public boolean isFileInFolder(File file, String folder) {
        return new CloudMainPage().destinationFolderClick(folder).getElementFile(file).isDisplayed();
    }

    public boolean isFolderCreated(String folderName) {
        return new CloudMainPage().isFolderCreated(folderName);
    }

    public boolean isFolderExist(String folderName) {
        return new CloudMainPage().isFolderExist(folderName);
    }

}
