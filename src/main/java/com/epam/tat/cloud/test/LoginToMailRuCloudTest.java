package com.epam.tat.cloud.test;

import com.epam.tat.cloud.exception.AuthenticationException;
import com.epam.tat.common.bo.AccountFactory;
import com.epam.tat.common.service.AuthenticationService;
import com.epam.tat.framework.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class LoginToMailRuCloudTest {
    private AuthenticationService authenticationService = new AuthenticationService();

    @Test(description = "Test sign in with correct login and password")
    public void testSignInWithCorrectLogin() {
        authenticationService.cloudSignInUsingLoginAndPassword(AccountFactory.getExistentAccount());
    }

    @Test(description = "Test sign in with incorrect login and password",
            expectedExceptions = AuthenticationException.class)
    public void testSignInWithIncorrectLogin() {
        authenticationService.cloudSignInUsingLoginAndPassword(AccountFactory.getInvalidAccount());
    }

    @Test(description = "Test sign in without password")
    public void testSignInWithoutPassword() {
        authenticationService.cloudSignInUsingLoginAndPassword(AccountFactory.getNoPasswordAccount());
        Assert.assertTrue(authenticationService.isPasswordBorderRed());
    }

    @Test(description = "Test sign in without login")
    public void testSignInWithoutLogin() {
        authenticationService.cloudSignInUsingLoginAndPassword(AccountFactory.getNoLoginAccount());
        Assert.assertTrue(authenticationService.isLoginBorderRed());
    }

    @AfterMethod
    public void closeDriver() {
        Browser.getInstance().stopBrowser();
    }
}
