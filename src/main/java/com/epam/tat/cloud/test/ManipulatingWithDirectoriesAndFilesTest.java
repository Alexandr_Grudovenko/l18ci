package com.epam.tat.cloud.test;

import com.epam.tat.common.bo.Account;
import com.epam.tat.common.bo.AccountFactory;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.utils.RandomUtil;
import com.epam.tat.common.service.AuthenticationService;
import com.epam.tat.cloud.service.FileService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;

public class ManipulatingWithDirectoriesAndFilesTest {
    private String folder = RandomUtil.getRandomFolder();
    private File file = RandomUtil.getRandomFile();
    private AuthenticationService authenticationService = new AuthenticationService();
    private Account account = AccountFactory.getExistentAccount();
    private FileService fileService = new FileService();

    @BeforeMethod
    public void setUpDriver() {
        authenticationService.cloudSignInUsingLoginAndPassword(account);
    }

    @AfterMethod
    public void closeDriver() {
        Browser.getInstance().stopBrowser();
    }

    @Test(description = "test creating new folder")
    public void testCreatingNewFolder() {
        fileService.createFolder(folder);
    }

    @Test(description = "test deleting folder", dependsOnMethods = "testDragAndDropFile")
    public void testDeletingFolder() {
        fileService.deleteFolder(folder);
    }

    @Test(description = "test upload file to the root directory")
    public void testUploadingFileToTheRootDirectory() {
        fileService.uploadFileToTheRootDirectory(file);
    }

    @Test(description = "Test drag and drop function", dependsOnMethods = "testShareLink")
    public void testDragAndDropFile() {
        fileService.dragAndDropFileToFolder(file, folder);
        Assert.assertTrue(fileService.isFileInFolder(file, folder));
    }

    @Test(description = "Test share link", dependsOnMethods = "testUploadingFileToTheRootDirectory")
    public void testShareLink() {
        fileService.shareFile(file);
    }
}
