package com.epam.tat.mail.exception;

public class EmailException extends RuntimeException {
    public EmailException(String message) {
        super(message);
    }
}
