package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class NewMailPage {
    private static Element composeToAddressField = new Element(xpath("//textarea[@data-original-name='To']"));
    private static Element subjectField = new Element(xpath("//input[@name='Subject']"));
    private static Element mailTextField = new Element(xpath("//body[@id='tinymce']"));
    private static Element sendMailButton = new Element(xpath("//div[@data-name='send']"));
    private static Element saveDraftButton = new Element(xpath("//div[@data-name='saveDraft']"));
    private static Element frame = new Element(xpath("//iframe[starts-with(@id,'toolkit-')]"));
    private static Element confirmButton =
            new Element(xpath("//div[@class='is-compose-empty_in']//button[@type='submit']"));

    public NewMailPage setAddress(String email) {
        composeToAddressField.type(email);
        return this;
    }

    public NewMailPage confirmButtonClick() {
        confirmButton.click();
        return this;
    }

    public NewMailPage saveDraft() {

        saveDraftButton.click();
        return this;
    }

    public NewMailPage setSubject(String subject) {
        subjectField.type(subject);
        return this;
    }

    public NewMailPage sendMail() {
        sendMailButton.click();
        return this;
    }

    public NewMailPage setMailText(String text) {
        frame.switchToFrame();
        mailTextField.type(text);
        frame.switchToDefaultContent();
        return this;
    }
}
