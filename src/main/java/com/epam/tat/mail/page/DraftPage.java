package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class DraftPage {
    private static final String DYNAMIC_DRAFT_MAIL_LOCATOR = "//div[contains(text(), '%s')]";
    private static final By TRASH_FOLDER_LOCATOR =
            By.xpath("//div[@id='b-nav_folders']//a[@href='/messages/trash/']");

    public void moveToTrash(String subject) {
        Element mail = new Element(xpath(String.format(DYNAMIC_DRAFT_MAIL_LOCATOR, subject)));
        mail.dragAndDropTo(TRASH_FOLDER_LOCATOR);
    }
}
