package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class LoginPageMail {
    public static String mailRuUrl = "https://mail.ru/";
    public static Element loginField = new Element(xpath("//input[@id='mailbox__login']"));
    public static Element passwordField = new Element(xpath("//input[@id='mailbox__password']"));
    public static Element submitButton = new Element(xpath("//input[@id='mailbox__auth__button']"));
    public static Element errorMessage = new Element(xpath("//div[@id='mailbox:authfail']"));

    public LoginPageMail open() {
        Browser.getInstance().navigate(mailRuUrl);
        return this;
    }

    public LoginPageMail setLogin(String login) {
        loginField.type(login);
        return this;
    }

    public LoginPageMail setPassword(String login) {
        passwordField.type(login);
        return this;
    }

    public void clickSignIn() {
        submitButton.click();
    }

    public String getErrorMessage() {
        return errorMessage.getText();
    }

    public boolean hasError() {
        return errorMessage.isDisplayed();
    }
}
