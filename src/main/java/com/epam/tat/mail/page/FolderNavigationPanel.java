package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class FolderNavigationPanel {
    private static Element inboxFolderLink =
            new Element(xpath("//div/a[@class='b-nav__link' and @href='/messages/inbox/']"));
    private static Element sentFolderLink =
            new Element(xpath("//div/a[@href='/messages/sent/']"));
    private static Element draftsFolderLink =
            new Element(xpath("//div[@class='b-nav__item js-href']//a[@href='/messages/drafts/']"));
    private static Element trashFolderLink =
            new Element(xpath("//div[@id='b-nav_folders']//a[@href='/messages/trash/']"));
    private static Element newLetterButton =
            new Element(xpath("//a[@data-name='compose']"));

    public FolderNavigationPanel newLetter() {
        newLetterButton.click();
        return this;
    }

    public void inboxFolderClick() {
        inboxFolderLink.click();
    }

    public void draftsFolderLinkClick() {
        draftsFolderLink.click();
    }

    public void sentFolderLinkClick() {
        sentFolderLink.click();
    }

    public void trashFolderLinkClick() {
        trashFolderLink.click();
    }

}
