package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class TrashPage {
    private static String dynamicMailLocator = "//div[contains(text(), '%')]";
    private static Element clearButton = new Element(xpath("//span[@data-name='clear-folder']"));
    private static Element clearConfirmButton =
            new Element(xpath("//div[@id='ScrollBodyInner']//button[@class='btn btn_main btn_stylish js-clearButton']"));

    public boolean isMailInTrash(String subject) {
        Element mail = new Element(xpath(String.format(dynamicMailLocator, subject)));
        return mail.isDisplayed();
    }

    public void clearTrash() {
        clearButton.click();
        clearConfirmButton.click();
    }
}
