package com.epam.tat.mail.page;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class MailPage {
    private static Element messageWithoutSubject = new Element(xpath("//div[contains(text(), 'Без темы')]"));
    private String letterWithSubjectLocator = "//a[@data-subject='%s']";

    public boolean isMailWithSubjectDisplayed(String subject) {
        Element letterWithSubject = new Element(xpath(String.format(letterWithSubjectLocator, subject)));
        return letterWithSubject.isDisplayed();
    }

    public boolean isMailWithoutSubjectDisplayed() {
        messageWithoutSubject.waitForAppear();
        return messageWithoutSubject.isDisplayed();
    }

    public String getErrorMessage() {
        return Browser.getInstance().getWrappedDriver().switchTo().alert().getText();
    }
}
