package com.epam.tat.mail.test;

import com.epam.tat.common.bo.Account;
import com.epam.tat.common.bo.AccountFactory;
import com.epam.tat.common.service.AuthenticationService;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.mail.exception.EmailException;
import com.epam.tat.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MailSendingTest {
    private Account account = AccountFactory.getExistentAccount();
    private MailService mailService = new MailService();

    @BeforeMethod
    public void setDriver() {
        new AuthenticationService().mailSignInUsingLoginAndPassword(account);
    }

    @Test(description = "Test to send mail with all fields filled")
    public void sendNewLetterWithAllFieldsTest() {
        mailService.sendNewMessage(account);
    }

    @Test(description = "Test to send mail without address", expectedExceptions = EmailException.class)
    public void sendNewLetterWithoutAddressTest() {
        mailService.sendMessageWithoutAddress();
    }

    @Test(description = "Test to send mail without subject")
    public void testSendNewLetterWithoutSubjectAndText() {
        mailService.sendMessageWithoutSubject(account);
    }

    @Test(description = "Sending mail without subject and body test")
    public void sendWithoutSubjectAndBodyTest() {
        mailService.sendWithoutSubjectAndBody(account);
        Assert.assertTrue(mailService.isMailInInboxAndSentFolder());
    }

    @Test(description = "Sending and deleting draft mail test")
    public void draftMailTest() {
        mailService.createAndDeleteDraftMail(account);
        Assert.assertTrue(mailService.isMailInTrash());
    }

    @Test(description = "Deleting draft mail from trash test", dependsOnMethods = "draftMailTest")
    public void draftMailDeleteFromTrashTest() {
        mailService.deleteDraftFromTrash();
        Assert.assertFalse(mailService.isMailInTrash());
    }

    @AfterMethod
    public void closeDriver() {
        Browser.getInstance().stopBrowser();
    }
}
