package com.epam.tat.mail.test;

import com.epam.tat.cloud.exception.AuthenticationException;
import com.epam.tat.common.bo.Account;
import com.epam.tat.common.bo.AccountFactory;
import com.epam.tat.common.service.AuthenticationService;
import com.epam.tat.framework.ui.Browser;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class LoginMailRuTest {
    private AuthenticationService authenticationService = new AuthenticationService();
    private Account account;

    @Test(description = "Test sign in with correct data")
    public void testSignInWithCorrectLogin() {
        account = AccountFactory.getExistentAccount();
        authenticationService.mailSignInUsingLoginAndPassword(account);
    }

    @Test(description = "Test sign in with icorrect data",
            expectedExceptions = AuthenticationException.class)
    public void testSignInWithIncorrectLoginAndPassword() {
        account = AccountFactory.getInvalidAccount();
        authenticationService.mailSignInUsingLoginAndPassword(account);
    }

    @Test(description = "Test sign in without password",
            expectedExceptions = AuthenticationException.class)
    public void testSignInWithEmptyPassword() {
        account = AccountFactory.getNoPasswordAccount();
        authenticationService.mailSignInUsingLoginAndPassword(account);
    }

    @Test(description = "Test sign in without login")
    public void testSignInWithEmptyLogin() {
        account = AccountFactory.getNoLoginAccount();
        authenticationService.mailSignInUsingLoginAndPassword(account);
    }

    @AfterMethod
    public void closeDriver() {
        Browser.getInstance().stopBrowser();
    }

}
