package com.epam.tat.mail.service;

import com.epam.tat.common.bo.Account;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.utils.RandomUtil;
import com.epam.tat.mail.exception.EmailException;
import com.epam.tat.mail.page.*;

public class MailService {
    private String subject = RandomUtil.getRandomSubject();

    public void sendNewMessage(Account account) {
        Log.debug("Try to send new message");
        FolderNavigationPanel navigationPanel = new FolderNavigationPanel();
        navigationPanel.newLetter();
        NewMailPage newMailPage = new NewMailPage();
        newMailPage
                .setAddress(account.getLogin())
                .setSubject(subject)
                .setMailText(RandomUtil.getRandomMailText())
                .sendMail();
        navigationPanel.inboxFolderClick();
        if (!(new MailPage().isMailWithSubjectDisplayed(subject))) {
            throw new EmailException("Mail didn't sent!");
        }
    }

    public void sendMessageWithoutAddress() {
        Log.debug("Try to send message without address");
        FolderNavigationPanel navigationPanel = new FolderNavigationPanel();
        navigationPanel.newLetter();
        NewMailPage newMailPage = new NewMailPage();
        newMailPage
                .setSubject(subject)
                .setMailText(RandomUtil.getRandomMailText())
                .sendMail();
        if (new MailPage().getErrorMessage().equals("Не указан адрес получателя")) {
            throw new EmailException("No address!");
        }
    }

    public void sendMessageWithoutSubject(Account account) {
        new FolderNavigationPanel().newLetter();
        new NewMailPage()
                .setAddress(account.getLogin())
                .setMailText(RandomUtil.getRandomMailText())
                .sendMail();
    }

    public void sendWithoutSubjectAndBody(Account account) {
        new FolderNavigationPanel().newLetter();
        new NewMailPage()
                .setAddress(account.getLogin())
                .sendMail()
                .confirmButtonClick();
    }

    public boolean isMailInInboxFolder() {
        new FolderNavigationPanel()
                .inboxFolderClick();
        return new MailPage().isMailWithoutSubjectDisplayed();
    }

    public boolean isMailInSentFolder() {
        new FolderNavigationPanel()
                .sentFolderLinkClick();
        return new MailPage().isMailWithoutSubjectDisplayed();
    }

    public void createAndDeleteDraftMail(Account account) {
        FolderNavigationPanel navigationPanel = new FolderNavigationPanel();
        navigationPanel.newLetter();
        NewMailPage newMailPage = new NewMailPage();
        newMailPage
                .setAddress(account.getLogin())
                .setSubject(subject)
                .setMailText(RandomUtil.getRandomMailText())
                .saveDraft();
        navigationPanel
                .draftsFolderLinkClick();
        new DraftPage().moveToTrash(subject);
        navigationPanel.trashFolderLinkClick();
    }

    public boolean isMailInTrash() {
        return new TrashPage().isMailInTrash(subject);
    }

    public void deleteDraftFromTrash() {
        new TrashPage().clearTrash();
    }

    public boolean isMailInInboxAndSentFolder() {
        return (isMailInSentFolder()) && (isMailInInboxFolder());
    }
}
