package com.epam.tat.common.service;

import com.epam.tat.cloud.exception.AuthenticationException;
import com.epam.tat.cloud.page.CloudMainPage;
import com.epam.tat.cloud.page.LoginMailPage;
import com.epam.tat.cloud.page.LoginPage;
import com.epam.tat.common.bo.Account;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.mail.page.LoginPageMail;

public class AuthenticationService {
    private final String redBorderColour = "rgb(206, 25, 0)";

    public void cloudSignInUsingLoginAndPassword(Account account) {
        Log.debug(String.format("Try to sign in with: [login: %s, password: %s]",
                account.getLogin(), account.getPassword()));
        LoginPage loginPage = new LoginPage().open();
        loginPage
                .clickMainSignIn()
                .setLogin(account.getLogin())
                .setPassword(account.getPassword())
                .clickSignIn();
        if (!new CloudMainPage().getUserName()) {
            throw new AuthenticationException(new LoginMailPage().getErrorMessage());
        }
    }

    public void mailSignInUsingLoginAndPassword(Account account) {
        Log.debug(String.format("Try to sign in with: [login: %s, password: %s]",
                account.getLogin(), account.getPassword()));
        LoginPageMail loginPageMail = new LoginPageMail().open();
        loginPageMail
                .setLogin(account.getLogin())
                .setPassword(account.getPassword())
                .clickSignIn();
        if (loginPageMail.hasError()) {
            throw new AuthenticationException(loginPageMail.getErrorMessage());
        }
    }

    public boolean isLoginBorderRed() {
        return new LoginPage().getLoginBorderColor().equals(redBorderColour);
    }

    public boolean isPasswordBorderRed() {
        return new LoginPage().getPasswordBorderColor().equals(redBorderColour);
    }
}
