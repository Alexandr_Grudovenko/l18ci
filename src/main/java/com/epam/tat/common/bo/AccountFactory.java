package com.epam.tat.common.bo;

import com.epam.tat.framework.utils.RandomUtil;

public class AccountFactory {
    public static Account getExistentAccount() {
        return new Account("tat2017gomel@mail.ru", "12345tat");
    }

    public static Account getInvalidAccount() {
        return new Account(RandomUtil.getRandomLogin(), RandomUtil.getRandomPassword());
    }

    public static Account getNoPasswordAccount() {
        return new Account(RandomUtil.getRandomLogin(), "");
    }

    public static Account getNoLoginAccount() {
        return new Account("", RandomUtil.getRandomPassword());
    }
}
